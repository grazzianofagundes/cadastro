@extends('layout.app', ["current" => "produtos"])
@section('body')

<div class="card border">
    <div class="card-body">
        <form action="/produtos" method="post">
            @csrf
            <div class="form-group">
                <label for="nomeProduto">Nome da Produto</label>
                <input type="text" class="form-control" name="nomeProduto" id="nomeProduto" placeholder="Produto">
                <label for="estoque">Estoque</label>
                <input type="text" class="form-control" name="estoque" id="estoque" placeholder="Estoque">
                <label for="preco">Preço R$</label>
                <input type="text" class="form-control" name="preco" id="preco" placeholder="Preço">
                <label for="categoria">Categorias</label>
                <select class="form-control" id="categoria" name="categoria">
                    <option value="">Escolha uma categoria</option>
                    @foreach ($categorias as $cat)
                        <option value="{{ $cat->id }}">{{ $cat->nome }}</option>
                    @endforeach                
                </select>
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            <button type="cancel" class="btn btn-danger btn-sm">Cancelar</button>
        </form>
    </div>
</div>
    
@endsection
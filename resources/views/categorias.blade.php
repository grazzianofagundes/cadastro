@extends('layout.app', ["current" => "categorias"])
@section('body')
<div class="card border">
    <div class="card-body">
        <h5 class="card-title">Categorias</h5>

        @if (count($cat) > 0)
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Categoria</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cat as $categoria)
                    <tr>
                        <td>{{ $categoria->id }}</td>
                        <td>{{ $categoria->nome }}</td>
                        <td>
                            <a href="/categorias/editar/{{ $categoria->id }}" class="btn btn-sm btn-warning">Editar</a>
                            <a href="/categorias/apagar/{{ $categoria->id }}" class="btn btn-sm btn-danger">Apagar</a>
                        </td>
                    </tr>                    
                    @endforeach
                </tbody>
            </table>
        @endif

    </div>

    <div class="card-footer">
        <a href="/categorias/novo" class="btn btn-sm btn-info">Cadastrar</a>
    </div>

</div>
    
@endsection